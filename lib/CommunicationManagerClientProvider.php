<?php

namespace Ensi\CommunicationManagerClient;

class CommunicationManagerClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\CommunicationManagerClient\Api\ThemesApi',
        '\Ensi\CommunicationManagerClient\Api\StatusesApi',
        '\Ensi\CommunicationManagerClient\Api\TypesApi',
        '\Ensi\CommunicationManagerClient\Api\EventsApi',
        '\Ensi\CommunicationManagerClient\Api\ChannelsApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\CommunicationManagerClient\Dto\ThemeFillableProperties',
        '\Ensi\CommunicationManagerClient\Dto\RequestBodyPagination',
        '\Ensi\CommunicationManagerClient\Dto\Type',
        '\Ensi\CommunicationManagerClient\Dto\OrderData',
        '\Ensi\CommunicationManagerClient\Dto\ChannelForPatch',
        '\Ensi\CommunicationManagerClient\Dto\ThemeForCreate',
        '\Ensi\CommunicationManagerClient\Dto\SearchStatusesResponseMeta',
        '\Ensi\CommunicationManagerClient\Dto\Error',
        '\Ensi\CommunicationManagerClient\Dto\SearchTypesRequest',
        '\Ensi\CommunicationManagerClient\Dto\SearchStatusesResponse',
        '\Ensi\CommunicationManagerClient\Dto\OrderEvent',
        '\Ensi\CommunicationManagerClient\Dto\Channel',
        '\Ensi\CommunicationManagerClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\CommunicationManagerClient\Dto\PaginationTypeEnum',
        '\Ensi\CommunicationManagerClient\Dto\ChannelFillableProperties',
        '\Ensi\CommunicationManagerClient\Dto\TypeForPatch',
        '\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse',
        '\Ensi\CommunicationManagerClient\Dto\SearchThemesResponse',
        '\Ensi\CommunicationManagerClient\Dto\ThemeForPatch',
        '\Ensi\CommunicationManagerClient\Dto\ChannelForCreate',
        '\Ensi\CommunicationManagerClient\Dto\SendInternalMessageEvent',
        '\Ensi\CommunicationManagerClient\Dto\ThemeResponse',
        '\Ensi\CommunicationManagerClient\Dto\StatusResponse',
        '\Ensi\CommunicationManagerClient\Dto\ResponseBodyPagination',
        '\Ensi\CommunicationManagerClient\Dto\BasketItem',
        '\Ensi\CommunicationManagerClient\Dto\SearchChannelsResponse',
        '\Ensi\CommunicationManagerClient\Dto\TypeReadonlyProperties',
        '\Ensi\CommunicationManagerClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\CommunicationManagerClient\Dto\TypeFillableProperties',
        '\Ensi\CommunicationManagerClient\Dto\ErrorResponse',
        '\Ensi\CommunicationManagerClient\Dto\UserTypeEnum',
        '\Ensi\CommunicationManagerClient\Dto\ChannelResponse',
        '\Ensi\CommunicationManagerClient\Dto\MailTypeEnum',
        '\Ensi\CommunicationManagerClient\Dto\StatusFillableProperties',
        '\Ensi\CommunicationManagerClient\Dto\StatusForPatch',
        '\Ensi\CommunicationManagerClient\Dto\SearchTypesResponse',
        '\Ensi\CommunicationManagerClient\Dto\Theme',
        '\Ensi\CommunicationManagerClient\Dto\SearchStatusesRequest',
        '\Ensi\CommunicationManagerClient\Dto\TypeForCreate',
        '\Ensi\CommunicationManagerClient\Dto\ChannelReadonlyProperties',
        '\Ensi\CommunicationManagerClient\Dto\TypeResponse',
        '\Ensi\CommunicationManagerClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\CommunicationManagerClient\Dto\RequestBodyCursorPagination',
        '\Ensi\CommunicationManagerClient\Dto\ModelInterface',
        '\Ensi\CommunicationManagerClient\Dto\SearchChannelsRequest',
        '\Ensi\CommunicationManagerClient\Dto\Status',
        '\Ensi\CommunicationManagerClient\Dto\SendEmailEventData',
        '\Ensi\CommunicationManagerClient\Dto\StatusForCreate',
        '\Ensi\CommunicationManagerClient\Dto\SendInternalMessageEventData',
        '\Ensi\CommunicationManagerClient\Dto\SearchThemesRequest',
        '\Ensi\CommunicationManagerClient\Dto\ThemeReadonlyProperties',
        '\Ensi\CommunicationManagerClient\Dto\SendEmailEvent',
        '\Ensi\CommunicationManagerClient\Dto\StatusReadonlyProperties',
    ];

    /** @var string */
    public static $configuration = '\Ensi\CommunicationManagerClient\Configuration';
}
