# # OrderEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Ensi\CommunicationManagerClient\Dto\MailTypeEnum**](MailTypeEnum.md) |  | [optional] 
**data** | [**\Ensi\CommunicationManagerClient\Dto\OrderData**](OrderData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


