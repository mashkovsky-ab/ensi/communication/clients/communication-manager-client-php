# # SendEmailEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Тип события | [optional] 
**data** | [**\Ensi\CommunicationManagerClient\Dto\SendEmailEventData**](SendEmailEventData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


