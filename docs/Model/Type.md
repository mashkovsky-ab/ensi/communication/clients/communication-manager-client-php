# # Type

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор статуса | [optional] 
**name** | **string** | Имя статуса | [optional] 
**active** | **bool** | Активность | [optional] 
**channel** | **int[]** | Каналы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


