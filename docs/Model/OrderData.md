# # OrderData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subject** | **string** |  | [optional] 
**to** | **string[]** |  | [optional] 
**order_number** | **string** |  | [optional] 
**create_date** | **string** |  | [optional] 
**receiver_name** | **string** |  | [optional] 
**receiver_email** | **string** |  | [optional] 
**receiver_phone** | **string** |  | [optional] 
**delivery_address** | **string** |  | [optional] 
**delivery_comment** | **string** |  | [optional] 
**delivery_date** | **string** |  | [optional] 
**delivery_time** | **string** |  | [optional] 
**delivery_cost** | **string** |  | [optional] 
**basket_items** | [**\Ensi\CommunicationManagerClient\Dto\BasketItem[]**](BasketItem.md) |  | [optional] 
**items_cost** | **string** | Суммарная стоимость товаров заказа | [optional] 
**order_cost** | **string** | Итоговая стоимость заказа | [optional] 
**total_count** | **int** | Суммарное количество товаров в заказе | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


