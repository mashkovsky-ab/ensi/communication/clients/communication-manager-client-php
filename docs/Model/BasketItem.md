# # BasketItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_id** | **string** | Артикул товара | [optional] 
**name** | **string** | Название товара | [optional] 
**image_id** | **float** | ID картинки товара в файловом сервисе | [optional] 
**price** | **string** | Цена товара | [optional] 
**qty** | **int** | Количество товара в корзине | [optional] 
**sum** | **string** | Суммарная цена товара в корзине | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


