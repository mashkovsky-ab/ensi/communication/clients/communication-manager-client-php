# Ensi\CommunicationManagerClient\StatusesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStatus**](StatusesApi.md#createStatus) | **POST** /statuses | Создание объекта типа Status
[**deleteStatus**](StatusesApi.md#deleteStatus) | **DELETE** /statuses/{id} | Удаление объекта типа Status
[**patchStatus**](StatusesApi.md#patchStatus) | **PATCH** /statuses/{id} | Обновления части полей объекта типа Status
[**searchStatuses**](StatusesApi.md#searchStatuses) | **POST** /statuses:search | Получить список статусов, удовлетворяющих условиям



## createStatus

> \Ensi\CommunicationManagerClient\Dto\StatusResponse createStatus($status_for_create)

Создание объекта типа Status

Создание объекта типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$status_for_create = new \Ensi\CommunicationManagerClient\Dto\StatusForCreate(); // \Ensi\CommunicationManagerClient\Dto\StatusForCreate | 

try {
    $result = $apiInstance->createStatus($status_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->createStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status_for_create** | [**\Ensi\CommunicationManagerClient\Dto\StatusForCreate**](../Model/StatusForCreate.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\StatusResponse**](../Model/StatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStatus

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse deleteStatus($id)

Удаление объекта типа Status

Удаление объекта типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteStatus($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->deleteStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStatus

> \Ensi\CommunicationManagerClient\Dto\StatusResponse patchStatus($id, $status_for_patch)

Обновления части полей объекта типа Status

Обновления части полей объекта типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$status_for_patch = new \Ensi\CommunicationManagerClient\Dto\StatusForPatch(); // \Ensi\CommunicationManagerClient\Dto\StatusForPatch | 

try {
    $result = $apiInstance->patchStatus($id, $status_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->patchStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **status_for_patch** | [**\Ensi\CommunicationManagerClient\Dto\StatusForPatch**](../Model/StatusForPatch.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\StatusResponse**](../Model/StatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStatuses

> \Ensi\CommunicationManagerClient\Dto\SearchStatusesResponse searchStatuses($search_statuses_request)

Получить список статусов, удовлетворяющих условиям

Поиск объектов типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_statuses_request = new \Ensi\CommunicationManagerClient\Dto\SearchStatusesRequest(); // \Ensi\CommunicationManagerClient\Dto\SearchStatusesRequest | 

try {
    $result = $apiInstance->searchStatuses($search_statuses_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->searchStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_statuses_request** | [**\Ensi\CommunicationManagerClient\Dto\SearchStatusesRequest**](../Model/SearchStatusesRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\SearchStatusesResponse**](../Model/SearchStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

