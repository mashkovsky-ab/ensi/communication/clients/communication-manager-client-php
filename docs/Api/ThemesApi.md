# Ensi\CommunicationManagerClient\ThemesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTheme**](ThemesApi.md#createTheme) | **POST** /themes | Создание объекта типа Theme
[**deleteTheme**](ThemesApi.md#deleteTheme) | **DELETE** /themes/{id} | Удаление объекта типа Theme
[**patchTheme**](ThemesApi.md#patchTheme) | **PATCH** /themes/{id} | Обновления части полей объекта типа Theme
[**searchThemes**](ThemesApi.md#searchThemes) | **POST** /themes:search | Получить список тем, удовлетворяющих условиям



## createTheme

> \Ensi\CommunicationManagerClient\Dto\ThemeResponse createTheme($theme_for_create)

Создание объекта типа Theme

Создание объекта типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$theme_for_create = new \Ensi\CommunicationManagerClient\Dto\ThemeForCreate(); // \Ensi\CommunicationManagerClient\Dto\ThemeForCreate | 

try {
    $result = $apiInstance->createTheme($theme_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->createTheme: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **theme_for_create** | [**\Ensi\CommunicationManagerClient\Dto\ThemeForCreate**](../Model/ThemeForCreate.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\ThemeResponse**](../Model/ThemeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteTheme

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse deleteTheme($id)

Удаление объекта типа Theme

Удаление объекта типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteTheme($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->deleteTheme: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchTheme

> \Ensi\CommunicationManagerClient\Dto\ThemeResponse patchTheme($id, $theme_for_patch)

Обновления части полей объекта типа Theme

Обновления части полей объекта типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$theme_for_patch = new \Ensi\CommunicationManagerClient\Dto\ThemeForPatch(); // \Ensi\CommunicationManagerClient\Dto\ThemeForPatch | 

try {
    $result = $apiInstance->patchTheme($id, $theme_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->patchTheme: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **theme_for_patch** | [**\Ensi\CommunicationManagerClient\Dto\ThemeForPatch**](../Model/ThemeForPatch.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\ThemeResponse**](../Model/ThemeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchThemes

> \Ensi\CommunicationManagerClient\Dto\SearchThemesResponse searchThemes($search_themes_request)

Получить список тем, удовлетворяющих условиям

Поиск объектов типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_themes_request = new \Ensi\CommunicationManagerClient\Dto\SearchThemesRequest(); // \Ensi\CommunicationManagerClient\Dto\SearchThemesRequest | 

try {
    $result = $apiInstance->searchThemes($search_themes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->searchThemes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_themes_request** | [**\Ensi\CommunicationManagerClient\Dto\SearchThemesRequest**](../Model/SearchThemesRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\SearchThemesResponse**](../Model/SearchThemesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

